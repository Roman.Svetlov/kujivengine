library Kujivengine;


interface

//uses UMap, UPreRender, UObject;

type
  Point = record
    x, y: double;
    static function operator+(a, b: point): point;
    begin
      Result.x := a.x + b.x;
      Result.y := a.y + b.y; 
    end;
    
    static function operator-(a, b: point): point;
    begin
      Result.x := a.x - b.x;
      Result.y := a.y - b.y; 
    end;
  end;



type
  Ray = record
  public 
    EndCoord: Point;
    BeginCoord: Point;
    function getA(): double;
    begin
      if ABS(EndCoord.x) >= ABS(EndCoord.y) then 
      begin
        result := arccos(EndCoord.x);
        if EndCoord.y < 0 then  result := System.Math.PI * 2 - result;
      end else 
      begin
        result := arcsin(EndCoord.y);
        if EndCoord.x < 0 then  result := System.Math.PI * 3 - result;
      end;
      
      
      
      
      
    end;
    
    procedure SetA(angle: double);
    begin
      while angle > System.Math.PI * 2 do angle -= System.Math.PI * 2;
      
      while angle < System.Math.PI * -2 do angle += System.Math.PI * 2;
      
      EndCoord.x := cos(angle);
      EndCoord.y := sin(angle);
    end;
  
  private 
    a: double;
  end;

type
  Camera = class
  
  public
    function getFoV(): double := Self.FoV;
    
    function getDirection(): ray := Self.Direction;
    
    procedure setFoV(angle: double);
    begin
      Self.FoV := angle;
    end;
    
    procedure setDirection(ray: Kujivengine.Ray);
    begin
      Self.Direction := ray;
    end;
  
  private 
    Direction: Ray;
    FoV: double;
  
  
  end;

type
  Map = class
  public 
    procedure CreateMap(size: integer);
    procedure SaveMap(FileName: string := 'CaveMap.map');
    procedure LoadMap(FileName: string := 'CaveMap.map');
    function GetMap(): array of byte;
  
  private 
    Map: array of byte; 
    Size: integer;
    b: array of byte;
    f: file;
    FileName: string;
  end;

type
  line_of_bars = record
    type_of_bar: array of byte;
    height: array of double;
  end;

type
  RayCasting = class
  public 
    function PreRender(Map: Kujivengine.Map; Cam: Kujivengine.Camera; width: integer; Distance: integer): line_of_bars;
    static function Intersection(ray1, ray2: Ray; var flag: boolean): point;
    static function RayCast(ray: Kujivengine.Ray; blockX, blockY: integer; distance: double): point;
  private 
    
    LoB: line_of_bars; //0 type 1 height
  end;

////////////////////////////////////////////////////////////////////////////////////////
implementation

static function RayCasting.Intersection(ray1, ray2: Ray; var flag: boolean): point;

begin
  var dir1 := ray1.EndCoord - ray1.BeginCoord;
  var dir2 := ray2.EndCoord - ray2.BeginCoord;
  
  //считаем уравнения прямых проходящих через отрезки
  var a1: double := -dir1.y;
  var b1: double := +dir1.x;
  var d1: double := -(a1 * ray1.BeginCoord.x + b1 * ray1.BeginCoord.y);
  
  var a2: double := -dir2.y;
  var b2: double := +dir2.x;
  var d2: double := -(a2 * ray2.BeginCoord.x + b2 * ray2.BeginCoord.y);
  
  //подставляем концы отрезков, для выяснения в каких полуплоскотях они
  var seg1_line2_start: double := a2 * ray1.BeginCoord.x + b2 * ray1.BeginCoord.y + d2;
  var seg1_line2_end: double := a2 * ray1.EndCoord.x + b2 * ray1.EndCoord.y + d2;
  
  var seg2_line1_start: double := a1 * ray2.BeginCoord.x + b1 * ray2.BeginCoord.y + d1;
  var seg2_line1_end: double := a1 * ray2.EndCoord.x + b1 * ray2.EndCoord.y + d1;
  
  
        //если концы одного отрезка имеют один знак, значит он в одной полуплоскости и пересечения нет.
  if ((seg1_line2_start * seg1_line2_end >= 0) OR (seg2_line1_start * seg2_line1_end >= 0)) then
    flag := false;
  
  var u: double := seg1_line2_start / (seg1_line2_start - seg1_line2_end);
  result.x := ray1.BeginCoord.x + (u * dir1.x);
  result.y := ray1.BeginCoord.y + (u * dir1.y);
  
  flag := true;
end;

static function RayCasting.RayCast(ray: Kujivengine.Ray; blockX, blockY: integer; distance: double): point;
var
  lineX, lineY: Kujivengine.Ray;
  point1, point2: Kujivengine.Point;
  flag: boolean;
begin
  if ray.BeginCoord.x < blockX then blockX += 1;
  if ray.BeginCoord.y < blockY then blockY += 1;
  
  lineX.BeginCoord.x := blockX;
  lineX.EndCoord.x := blockX;
  lineX.BeginCoord.y := 0;
  lineX.EndCoord.y := 1;
  
  lineY.BeginCoord.y := blockY;
  lineY.EndCoord.y := blockY;
  lineY.BeginCoord.x := 0;
  lineY.EndCoord.x := 1;
  
  point1 := RayCasting.Intersection(ray, lineX, flag);
  point2 := RayCasting.Intersection(ray, lineY, flag);
  if Power(point1.x - ray.BeginCoord.x, 2) + Power(point1.y - ray.BeginCoord.y, 2) < Power(point2.x - ray.BeginCoord.x, 2) + Power(point2.y - ray.BeginCoord.y, 2) then
    result := point1 else result := point2;
end;

procedure Map.CreateMap(Size: integer);
var
  all: byte;
  
  {procedure cave;
  begin
    for x: integer := 0 to (Size - 1) do
      for y: integer := 0 to (Size - 1) do
      begin
        for xm: integer := -1 to 1 do
          for ym: integer := -1 to 1 do
            if ((((x + xm) + (y + ym) * Size) < (Size * Size)) and (((x + xm) + (y + ym) * Size) >= 0)) then 
              if ((xm <> 0) or (ym <> 0)) then
              begin
                if Self.Map[(x + xm) + (y + ym) * Size] = 1 then all += 1;
              end;
  
        if all >= 5 then Self.b[x + y * Size] := 1;   
        if all < 3 then Self.b[x + y * Size] := 0;
  
        all := 0;
      end;
  
    for x: integer := 0 to (Size - 1) do
      for y: integer := 0 to (Size - 1) do
        Self.Map[x + y * Size] := Self.b[x + y * Size];
  end;}
  
  procedure cave;
  begin
    for x: integer := 0 to (Size - 1) do
      for y: integer := 0 to (Size - 1) do
      begin
        for xm: integer := -1 to 1 do
          for ym: integer := -1 to 1 do
            if ((((x + xm) + (y + ym) * Size) < (Size * Size)) and (((x + xm) + (y + ym) * Size) >= 0)) then 
              if ((xm <> 0) or (ym <> 0)) then
              begin
                if Self.Map[(x + xm) + (y + ym) * Size] = 1 then all += 1;
              end;
        
        if all >= 5 then Self.b[x + y * Size] := 1;   
        if all < 3 then Self.b[x + y * Size] := 0;
        
        all := 0;
      end;
    
    for x: integer := 0 to (Size - 1) do
      for y: integer := 0 to (Size - 1) do
        Self.Map[x + y * Size] := Self.b[x + y * Size];
  end;
  
  procedure cave2;
  begin
    for x: integer := 0 to (Size - 1) do
      for y: integer := 0 to (Size - 1) do
      begin
        for xm: integer := -1 to 1 do
          for ym: integer := -1 to 1 do
            if ((((x + xm) + (y + ym) * Size) < (Size * Size)) and (((x + xm) + (y + ym) * Size) >= 0)) then 
              if ((xm <> 0) or (ym <> 0)) then
              begin
                if Self.Map[(x + xm) + (y + ym) * Size] = 1 then all += 1;
              end;
        
        if Self.Map[x + y * Size] = 1 then 
        begin
          if all >= 4 then Self.b[x + y * Size] := 1;
          if all < 2 then Self.b[x + y * Size] := 0;
        end 
        else
        if all >= 5 then Self.b[x + y * Size] := 1;
        
        
        
        all := 0;
      end;
    
    for x: integer := 0 to (Size - 1) do
      for y: integer := 0 to (Size - 1) do
        Self.Map[x + y * Size] := Self.b[x + y * Size];
  end;

var
  border: integer := 1;
begin
  self.Size := Size;
  SetLength(Self.Map, Size * Size);
  SetLength(Self.b, Size * Size);
  SetLength(Self.Map, Size * Size);
  
  for x: integer := 0 to (Size - 1) do
    for y: integer := 0 to (Size - 1) do
    begin
      if ((random(1, 100) < 47)) then Self.Map[x + y * Size] := 1;//56//47
      if x <= 0 + border then Self.Map[x + y * Size] := 1;
      if y <= 0 + border then Self.Map[x + y * Size] := 1;
      if y >= size - 1 - border then Self.Map[x + y * Size] := 1;
      if x >= size - 1 - border then Self.Map[x + y * Size] := 1;
    end;
  
  
  for i: integer := 1 to 5 do cave2;
  for i: integer := 1 to 3 do cave;
  
  
  //for i: integer := 1 to 6 do cave;
  
end;

procedure Map.SaveMap(FileName: string);
begin
  assign(f, FileName);
  f.Rewrite;
  f.WriteBytes(Self.Map);
  f.Close;
end;

procedure Map.LoadMap(FileName: string);
begin
  assign(f, FileName);
  f.Reset;
  Self.Map := f.ReadBytes(f.Size);
  f.Close;
end;

function Map.GetMap(): array of byte;
begin
  result := Self.Map;
end;

function RayCasting.PreRender(Map: Kujivengine.Map; Cam: Kujivengine.Camera; width: integer; Distance: integer): line_of_bars;

var
  ray: Kujivengine.Ray;
// 1/d = относительная высота
var
  collision: boolean := false;
var
  step: double := 0.1;
begin
  
  SetLength(result.height, width);
  SetLength(result.type_of_bar, width);
  
  ray.SetA(cam.getDirection.getA - (cam.getFoV / 2));
  
  
  for var bar: integer := 0 to width - 1 do
  begin
    ray.BeginCoord := cam.getDirection.BeginCoord;
    collision := false;
    
    while (
    (
    (collision = false) 
    and 
    (sqrt(Power((ray.BeginCoord.x - cam.getDirection.BeginCoord.x), 2) + Power((ray.BeginCoord.y - cam.getDirection.BeginCoord.y), 2)) <= Distance))
    and
    (
    ((sqrt(Map.GetMap.Length) * Round(ray.BeginCoord.y + step * sin(ray.getA)) + Round(ray.BeginCoord.x + step * cos(ray.getA))) >= 0)
    and 
    ((sqrt(Map.GetMap.Length) * Round(ray.BeginCoord.y + step * sin(ray.getA)) + Round(ray.BeginCoord.x + step * cos(ray.getA))) < Map.GetMap.Length))) do
    begin
      
      
      ray.BeginCoord.x += step * cos(ray.getA);
      ray.BeginCoord.y += step * sin(ray.getA);
      
      
      
      if Map.GetMap[sqrt(Map.GetMap.Length).Round * ray.BeginCoord.y.Round + ray.BeginCoord.x.Round] <> 0 then
      begin
        collision := true;    
        result.type_of_bar[bar] := Map.GetMap[sqrt(Map.GetMap.Length).Round * ray.BeginCoord.y.Round + ray.BeginCoord.x.Round];
        //result.height[bar] := distance / sqrt(ABS(Power((ray.BeginCoord.x - cam.getDirection.BeginCoord.x), 2) + Power((ray.BeginCoord.y - cam.getDirection.BeginCoord.y), 2)));
        result.height[bar] := 1 / sqrt(ABS(Power((ray.BeginCoord.x - cam.getDirection.BeginCoord.x), 2) + Power((ray.BeginCoord.y - cam.getDirection.BeginCoord.y), 2)));
        // /(cos((cam.getDirection.getA-ray.getA)));
      end else result.type_of_bar[bar] := 0;
      
      
    end;
    
    ray.SetA(ray.getA + (cam.getFoV / width));
  end;
  
end;


end.