{$reference Kujivengine.dll}
uses graphabc;

const
  size = 256;
  
const
  scale = 4;

const
  width = 1770 div scale;//1770

const
  height = 980;//980


const
  an = 0;

const
  fo = 90;

const
  distance = 16;

var
  ray: Kujivengine.Ray := new Ray;

var
  cam: Kujivengine.Camera := new Camera;

var
  Pic: Picture;

var
  lob: Kujivengine.line_of_bars := new line_of_bars;

procedure KeyDown(Key: integer);
begin
  if Key = VK_Left then
  begin
    ray.SetA(ray.getA() + DegToRad(-6));
    cam.setDirection(ray);
  end; 
  if Key = VK_Right then
  begin
    ray.SetA(ray.getA() + DegToRad(+6));
    cam.setDirection(ray);
  end; 
  if Key = VK_Up then
  begin
    ray.BeginCoord.x += cos(ray.getA) * 1;
    ray.BeginCoord.y += sin(ray.getA) * 1;
    cam.setDirection(ray);
  end; 
  if Key = VK_Down then
  begin
    ray.BeginCoord.x -= cos(ray.getA) * 1;
    ray.BeginCoord.y -= sin(ray.getA) * 1;
    cam.setDirection(ray);
  end; 
  
end;


begin
  if scale > 0 then 
    window.SetSize(width*scale, height) 
  else
     window.SetSize(width, height);
  
  window.CenterOnScreen;
  var map: Kujivengine.Map := new Map;
  if scale > 0 then 
    pic := new Picture(width*scale, height) 
  else
       pic := new Picture(width, height);

  
  map.CreateMap(size);
  //map.SaveMap('test');
  //map.LoadMap('test');
  
  var a := map.GetMap;
  
  lockdrawing;
  for var x: integer := 0 to size - 1 do
  begin
    for var y: integer := 0 to size - 1 do
    begin
      if a[x + y * size] = 1 then pic.setpixel(x, y, Color.Gray);
    end;
  end;
  pic.Draw(0, 0);
  redraw;
  //64 30
  
  ray.BeginCoord.x := size div 2 - 0.5;//64
  ray.BeginCoord.y := size div 2 - 0.5;//30
  ray.SetA(DegToRad(an));
  
  while map.GetMap[(ray.BeginCoord.x + ray.BeginCoord.y * size).Round] <> 0 do
  begin
    ray.BeginCoord.x := random(size);
    ray.BeginCoord.y := random(size);
  end;
  
  cam.setDirection(ray);
  
  cam.setFoV(DegToRad(fo));
  
  var pr: Kujivengine.RayCasting := new RayCasting;
  {println(map.GetMap);
  println(pr.PreRender(Map,cam,width,64));
  println(cam.getFoV,cam.getDirection,ray.getA);
  println();}
  
  Pen.Color := Color.Blue;
  line(
  cam.getDirection.BeginCoord.x.Round, cam.getDirection.BeginCoord.y.Round,
  Round(cos(cam.getDirection.getA - (cam.getFoV / 2))) * distance,
  Round(sin(cam.getDirection.getA - (cam.getFoV / 2))) * distance);
  Pen.Color := Color.Red;
  line(
  cam.getDirection.BeginCoord.x.Round, cam.getDirection.BeginCoord.y.Round,
  Round(cos(cam.getDirection.getA + (cam.getFoV / 2))) * distance,
  Round(sin(cam.getDirection.getA + (cam.getFoV / 2))) * distance);
  Pen.Color := Color.Black;
  line(
  cam.getDirection.BeginCoord.x.Round, cam.getDirection.BeginCoord.y.Round,
  Round(cos(cam.getDirection.getA)) * distance,
  Round(sin(cam.getDirection.getA)) * distance);
  
  
  redraw;
  
  
  lob := pr.PreRender(Map, cam, width, distance);
  
  
  ray.SetA(DegToRad(0));
  cam.setDirection(ray);
  
  {while true do
  begin
    Pen.Color := Color.Black;
  
    line(
    window.Width div 2, window.Height div 2,
    window.Width div 2 + Round(cos(cam.getDirection.getA) * 64),
    window.Height div 2 + Round(sin(cam.getDirection.getA) * 64));
  
    ray.SetA(ray.getA() + DegToRad(2));
    cam.setDirection(ray);
    window.Caption := RadToDeg(cam.getDirection.getA).ToString;
    redraw;
    //sleep(100);
  end;}
  var d: DateTime;
  var odd: integer := 0;
  
  
  while true do
  begin
    odd += 1;
    d := DateTime.Now;
    
    lockdrawing;
    //if odd mod 2 = 0 then window.Clear;
    window.Clear;
    
    if scale > 0 then 
    begin
      brush.Color := color.LightGreen;
      Pen.Color := color.LightGreen;
      pic.Rectangle(0, height div 2, width * scale, height);
      
      brush.Color := color.SkyBlue;
      Pen.Color := color.SkyBlue;
      pic.Rectangle(0, 0, width * scale, height div 2);
    end else
    begin
      brush.Color := color.LightGreen;
      Pen.Color := color.LightGreen;
      pic.Rectangle(0, height div 2, width, height);
      
      brush.Color := color.SkyBlue;
      Pen.Color := color.SkyBlue;
      pic.Rectangle(0, 0, width, height div 2);
    end;
    
    
    {brush.Color := color.DimGray;
    Pen.Color := color.DarkGray;
    pic.Rectangle(0, height div 2, width, height);
    
    brush.Color := color.DarkGray;
    Pen.Color := color.DarkGray;
    pic.Rectangle(0, 0, width, height div 2);}
    
    OnKeyDown := KeyDown;
    
    
    
    
    
    for var bar: integer := 0 to width - 1 do
    begin
      
      
      {Pen.Color := color.Gray;
      line(
      bar, Round(256 + height - lob.height[bar] * 100 / 2),
      bar, Round(256 + height + lob.height[bar] * 100 / 2));}
      
      brush.Color := RGB(round(64 + lob.height[bar] * 255), round(64 + lob.height[bar] * 255), round(64 + lob.height[bar] * 255));
      Pen.Color := RGB(round(64 + lob.height[bar] * 255), round(64 + lob.height[bar] * 255), round(64 + lob.height[bar] * 255));
      
      if scale > 0 then
      begin
        if lob.type_of_bar[bar] = 1 then 
          pic.Rectangle(
          bar * scale, Round(height div 2 - lob.height[bar] * (height div 2)),
          bar * scale + scale, Round(height div 2 + lob.height[bar] * (height div 2)));
      end else 
      begin
        if lob.type_of_bar[bar] = 1 then 
          pic.Rectangle(
          bar, Round(height div 2 - lob.height[bar] * (height div 2)),
          bar, Round(height div 2 + lob.height[bar] * (height div 2)));
      end;
      
      
      
      {if ((odd mod 2 = 0) and (bar mod 2 = 0)) then
      if lob.type_of_bar[bar] = 1 then 
      pic.Rectangle(
      bar, Round(height div 2 - lob.height[bar] * (height div 2)),
      bar, Round(height div 2 + lob.height[bar] * (height div 2)));
      
      if ((odd mod 2 = 1) and (bar mod 2 = 1)) then
      if lob.type_of_bar[bar] = 1 then 
      pic.Rectangle(
      bar, Round(height div 2 - lob.height[bar] * (height div 2)),
      bar, Round(height div 2 + lob.height[bar] * (height div 2)));}
      
      {if (lob.height[bar] < 2/distance) then
      begin
      for var i:integer:=(height div 2 - lob.height[bar] * (height div 2)).Round to (height div 2 + lob.height[bar] * (height div 2)).Round do
      begin
      
      if random(100) < 16 then pic.SetPixel(bar,i,color.Red);
      end;
      end;}//туман?
      
      {Rectangle(
      bar*scale, Round(height div 2 - lob.height[bar] * (height div 2)),
      bar*scale+scale, Round(height div 2 + lob.height[bar] * (height div 2)));}
      
    end;
    //window.Caption := RadToDeg(cam.getDirection.getA).ToString;
    
    
    {for var x: integer := cam.getDirection.BeginCoord.x.Round - distance to cam.getDirection.BeginCoord.x.Round + distance do
    begin
      for var y: integer := cam.getDirection.BeginCoord.y.Round - distance to cam.getDirection.BeginCoord.y.Round + distance do
      begin
        if a[x + y * size] = 1 then pic.setpixel(x, y, Color.DarkGray) else pic.setpixel(x, y, Color.LightGray);
      end;
    end;}
    
    for var x: integer := 0 to size - 1 do
    begin
      for var y: integer := 0 to size - 1 do
      begin
        if a[x + y * size] = 1 then pic.setpixel(x, y, Color.DarkGray) else pic.setpixel(x, y, Color.LightGray);
      end;
    end;
    
    
    Pen.Color := Color.Red;
    Brush.Color := Color.Red;
    pic.circle(cam.getDirection.BeginCoord.x.Round, cam.getDirection.BeginCoord.y.Round, 2);
    
    Pen.Color := Color.Blue;
    pic.line(
    cam.getDirection.BeginCoord.x.Round, cam.getDirection.BeginCoord.y.Round,
    cam.getDirection.BeginCoord.x.Round + Round(cos(cam.getDirection.getA - (cam.getFoV / 2)) * distance),
    cam.getDirection.BeginCoord.y.Round + Round(sin(cam.getDirection.getA - (cam.getFoV / 2)) * distance));
    Pen.Color := Color.Red;
    pic.line(
    cam.getDirection.BeginCoord.x.Round, cam.getDirection.BeginCoord.y.Round,
    cam.getDirection.BeginCoord.x.Round + Round(cos(cam.getDirection.getA + (cam.getFoV / 2)) * distance),
    cam.getDirection.BeginCoord.y.Round + Round(sin(cam.getDirection.getA + (cam.getFoV / 2)) * distance));
    
    Pen.Color := Color.Black;
    pic.line(
    cam.getDirection.BeginCoord.x.Round, cam.getDirection.BeginCoord.y.Round,
    cam.getDirection.BeginCoord.x.Round + Round(cos(cam.getDirection.getA) * distance),
    cam.getDirection.BeginCoord.y.Round + Round(sin(cam.getDirection.getA) * distance));
    
    Pen.Color := Color.Yellow;
    pic.Arc(cam.getDirection.BeginCoord.x.Round,cam.getDirection.BeginCoord.y.Round,Distance, 
    360-RadToDeg(cam.getDirection.getA + (cam.getFoV / 2)).Round,
    360-RadToDeg(cam.getDirection.getA - (cam.getFoV / 2)).Round);
   
    pic.draw(0, 0);
    
    //FloodFill(cam.getDirection.BeginCoord.x.Round, cam.getDirection.BeginCoord.y.Round,color.Red);
    
    //if odd mod 2 = 0 then pic.Clear;
    pic.Clear;
    redraw;
    
    
    
    //ray.SetA(ray.getA() + DegToRad(1));
    //cam.setDirection(ray);
    lob := pr.PreRender(Map, cam, width, distance);
    
    
    
    window.Caption := (1000 / (DateTime.Now - d).Milliseconds).ToString;
    //sleep(100);
  end;
end.
